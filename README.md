# DigitalCitizenship



## Lecturas importatntes que resaltar

-about circuit selectroni
A Defi nition of Digital Citizenship
Digital citizenship can be defi ned as
the norms of behavior with regard
to technology use. As a way of un-
derstanding the complexity of digital
citizenship and the issues of technol-
ogy use, abuse, and misuse, we have
identifi ed nine general areas of behav-
ior that make up digital citizenship.
1. Etiquette: electronic standards of
conduct or procedure
2. Communication: electronic ex-
change of information
3. Education: the process of teaching
and learning about technology and
the use of technology
4. Access: full electronic participation
in society
5. Commerce: electronic buying and
selling of goods
6. Responsibility: electronic responsi-
bility for actions and deeds
7. Rights: those freedoms extended to
everyone in a digital world
8. Safety: physical well-being in a
digital technology world
9. Security (self-protection): electronic
precautions to guarantee safety.

- we don't care about our data

Second, we should not assume our personal information is trivial and would not interest anyone. Time after time we have witnessed how our digital traces can be valuable to malicious individuals or large corporations.

Two decades since the first privacy paradox studies were conducted and despite a great deal of research, there is still a mismatch between people’s stated privacy concerns and their protective behaviours. How can we improve this?

- IA just copy what we leave sat social media

You answer a random call from a family member, and they breathlessly explain how there’s been a horrible car accident. They need you to send money right now, or they’ll go to jail. You can hear the desperation in their voice as they plead for an immediate cash transfer. While it sure sounds like them, and the call came from their number, you feel like something’s off. So, you decide to hang up and call them right back. When your family member picks up your call, they say there hasn’t been a car crash, and that they have no idea what you’re talking about.

Congratulations, you just successfully avoided an artificial intelligence scam call.

- Laws of protection of data

The CCPA governs any company doing business in
California that meets certain minimum thresholds,
including companies with websites accessible there. The
law provides consumers with three main “rights.” First,
consumers have a right to know information that businesses
have collected or sold about them, requiring businesses to
inform consumers about the personal data being collected.
Second, the CCPA provides consumers with a right to opt
out of the sale of their personal information. Third, the
CCPA gives consumers the right, in certain cases, to
request that a business delete any information collected
about the consumer (i.e., right to delete). The CCPA is
enforced via civil penalties in enforcement actions brought
by the California attorney general.

- Foreign Data Protection Law

The regulation also includes data breach
notification requirements, data security standards, and
conditions for cross-border data flows outside the EU.



resources:
1. https://www.wired.com/story/how-to-protect-yourself-ai-scam-calls-detect/
2. https://crsreports.congress.gov/product/pdf/IF/IF11207
